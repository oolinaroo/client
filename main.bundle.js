webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div style=\"text-align:center\">\n  <h1>\n    Welcome to {{title}}!\n  </h1>\n  <img width=\"100\"\n       src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg==\">\n</div>\n<!--\n<h2>Here are some links to help you start: </h2>\n<ul>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://angular.io/tutorial\">Tour of Heroes</a></h2>\n  </li>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://github.com/angular/angular-cli/wiki\">CLI Documentation</a></h2>\n  </li>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://blog.angular.io/\">Angular blog</a></h2>\n  </li>\n</ul>\n-->\n<div>\n  <app-ticker></app-ticker>\n</div>\n<div>\n  <app-order></app-order>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ticker_ticker_component__ = __webpack_require__("../../../../../src/app/ticker/ticker.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__order_order_component__ = __webpack_require__("../../../../../src/app/order/order.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["G" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */], __WEBPACK_IMPORTED_MODULE_7__ticker_ticker_component__["a" /* TickerComponent */], __WEBPACK_IMPORTED_MODULE_8__order_order_component__["a" /* OrderComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientModule */], __WEBPACK_IMPORTED_MODULE_2__angular_common__["a" /* CommonModule */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot()
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/order/order.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\r\n.demo-tab-group {\r\n  border: 1px solid #e8e8e8;\r\n}\r\n\r\n.demo-tab-content {\r\n  padding: 16px;\r\n}\r\n\r\n.table td {\r\n  padding: .25rem;\r\n}\r\n*/\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/order/order.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"well well-lg\">\n    <h2>Selected pair: <strong>{{SelectedPair}}</strong></h2>\n  </div>\n  <div>\n    <ng-container *ngFor=\"let caption of captions; let k = index\">\n      <table class=\"table table-sm table-striped table-hover table-bordered\"\n             [ngStyle]=\"{'float':k===0 ? 'left': 'right', 'width': '50%'}\">\n        <caption>{{caption}}</caption>\n        <thead>\n        <tr>\n          <th *ngFor=\"let column of columns; let i = index\">{{column}}</th>\n        </tr>\n        </thead>\n        <tbody>\n        <ng-container *ngIf=\"[ asks, bids] as asksbids\">\n          <tr *ngFor=\"let items of asksbids[k]; let i = index\" style=\"font-size: 9pt;\">\n            <td>{{i + 1}}</td>\n            <td *ngFor=\"let item of items; let j = index\">{{item}}</td>\n            <td *ngIf=\"k===0; then asksBlock else bidsBlock\">Volume</td>\n            <ng-template #asksBlock>{{getAskVolume(i)}}</ng-template>\n            <ng-template #bidsBlock>{{getBidVolume(i)}}</ng-template>\n          </tr>\n        </ng-container>\n        </tbody>\n      </table>\n    </ng-container>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/order/order.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OrderComponent = (function () {
    function OrderComponent(http) {
        this.http = http;
        this.captions = ['Sell', 'Buy'];
        this.columns = ['#', 'Price', 'Volume', 'Price * Volume'];
        this.arrs = ['Asks', 'Bids'];
    }
    OrderComponent.prototype.getOrders = function () {
        var _this = this;
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].apiEndpoint + '/returnOrderBook/USDT_BTC/10', { responseType: 'json' })
            .subscribe(function (data) {
            _this.keys = Object.keys(data);
            Object.keys(data).map(function (key) {
                switch (key) {
                    case 'asks':
                        _this.asks = data[key];
                        break;
                    case 'bids':
                        _this.bids = data[key];
                        break;
                }
            });
            // console.log(this.asks);
            // console.log(this.bids);
        });
    };
    OrderComponent.prototype.getAskVolume = function (index) {
        var volume = 0;
        volume = this.asks[index][0] * this.asks[index][1];
        return volume;
    };
    OrderComponent.prototype.getBidVolume = function (index) {
        var volume = 0;
        volume = this.bids[index][0] * this.bids[index][1];
        return volume;
    };
    OrderComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.getOrders();
        this.timer = __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["a" /* Observable */].timer(5000, 500);
        this.timer.subscribe(function (t) { return _this.getOrders(); });
    };
    OrderComponent.prototype.onclick = function () {
        // this.getOrders();
    };
    OrderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-order',
            template: __webpack_require__("../../../../../src/app/order/order.component.html"),
            styles: [__webpack_require__("../../../../../src/app/order/order.component.css")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* ViewEncapsulation */].None
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], OrderComponent);
    return OrderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/ticker/ticker.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*\r\n.demo-tab-group {\r\n  border: 1px solid #e8e8e8;\r\n}\r\n\r\n.demo-tab-content {\r\n  padding: 16px;\r\n}\r\n\r\n.table td {\r\n  padding: .25rem;\r\n}\r\n*/\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/ticker/ticker.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n  <!--<div style=\"font-size: 9pt; font-weight: normal;\">-->\n  <div>\n    <ngb-tabset type=\"pills\">\n      <ngb-tab title=\"{{pair}} ({{pairsDescr[n]}})\" *ngFor=\"let pair of pairs; let n = index\">\n        <ng-template ngbTabContent>\n          <table class=\"table table-sm table-striped table-hover table-bordered\">\n            <thead>\n            <tr>\n              <th *ngFor=\"let column of columns; let i = index\">{{column}}</th>\n            </tr>\n            </thead>\n            <tbody>\n            <ng-container *ngFor=\"let key of keys; let i = index\">\n              <tr *ngIf=\"key.indexOf(pair+'_')>-1 || pair=='ALL'\" (click)=\"setClickedPair(i)\" style=\"font-size: 9pt;\"\n                  [class.active]=\"i == selectedPair\">\n                <td>{{i + 1}}</td>\n                <td>{{key}}</td>\n                <td>{{tickers[i].id}}</td>\n                <td>{{tickers[i].last}}</td>\n                <td>{{tickers[i].lowestAsk}}</td>\n                <td>{{tickers[i].highestBid}}</td>\n                <td>{{tickers[i].percentChange}}</td>\n                <td>{{tickers[i].baseVolume}}</td>\n                <td>{{tickers[i].quoteVolume}}</td>\n                <td><input type=\"checkbox\" [disabled]=\"true\" [checked]=\"tickers[i].isFrozen==1\"></td>\n                <td>{{tickers[i].high24hr}}</td>\n                <td>{{tickers[i].low24hr}}</td>\n              </tr>\n            </ng-container>\n            </tbody>\n          </table>\n        </ng-template>\n      </ngb-tab>\n    </ngb-tabset>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/ticker/ticker.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TickerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TickerComponent = (function () {
    function TickerComponent(http) {
        this.http = http;
        this.pairs = ['ALL', 'BTC', 'ETH', 'XMR', 'USDT'];
        this.pairsDescr = ['All pairs', 'Bitcoin', 'Ethereum', 'Monero', 'Tether'];
        this.columns = ['#', 'Pair', 'ID', 'Last', 'Lowest Ask', 'Highest Bid', 'Percent Change', 'Base Volume',
            'Quote Volume', 'Is Frozen', 'High 24hr', 'Low 24hr'];
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' });
        this.setClickedRow = function (index) {
            this.selectedRow = index;
        };
    }
    TickerComponent.prototype.getTickers = function () {
        var _this = this;
        this.http.get(__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].apiEndpoint + '/returnTicker', { responseType: 'json' })
            .subscribe(function (data) {
            _this.keys = Object.keys(data);
            _this.tickers = Object.values(data);
            // console.log(this.tickers);
        });
    };
    TickerComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.getTickers();
        this.timer = __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["a" /* Observable */].timer(5000, 500);
        this.timer.subscribe(function (t) { return _this.getTickers(); });
    };
    TickerComponent.prototype.onclick = function () {
        // this.getTickers();
    };
    TickerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-ticker',
            template: __webpack_require__("../../../../../src/app/ticker/ticker.component.html"),
            styles: [__webpack_require__("../../../../../src/app/ticker/ticker.component.css")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* ViewEncapsulation */].None
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* NgModule */])({
            declarations: [],
            imports: [__WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], TickerComponent);
    return TickerComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    // apiEndpoint: 'http://localhost:3000/api/poloniex'
    apiEndpoint: 'http://poloniexapi-poloniexapi.7e14.starter-us-west-2.openshiftapps.com/api/poloniex'
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map